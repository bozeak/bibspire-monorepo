const _ = require('lodash');
const axios = require('axios');

const {caches} = require('../util/cache');
const {sleep} = require('../util/util');

const {getWork, workCount, pid2wid, manifest} = require('./metadata');
const {get_popularity} = require('./recommender_webtrekk');

axios.defaults.headers.post['Content-Type'] = 'application/x-ndjson';
async function index() {
  //await sleep(3000);
  //await rebuildManifestCache()
  //
  const count = await workCount();
  let works = new Set();

  console.time('rebuildManifestCache');
  //  await caches.manifest.flushdb();
  //  await caches.pid2wid.flushdb();
  let step = 0;
  console.time(`rebuildManifestCache step ${step} / ${count}`);
  for (let i = 1; i < count; ) {
    const promises = [];
    let j;
    for (j = 0; j < 5 && i < count; ++j) {
      promises.push(
        (async () =>
          works.add(
            await pid2wid((await getWork(i + j)).hasPart[0].pid)
          ))()
      );
    }
    i += j;
    await Promise.all(promises);

    if (i % 10000 === 1) {
      console.timeEnd(`rebuildManifestCache step ${step} / ${count}`);
      step = i - 1;
      console.log('bib', 'actual works', works.size, '/', i);
      console.time(`rebuildManifestCache step ${step} / ${count}`);
    }
  }
  console.timeEnd(`rebuildManifestCache step ${step} / ${count}`);
  console.timeEnd('rebuildManifestCache');

  works = Array.from(works);
  console.log('bib', 'actual works', works, count);

  //await axios.delete('http://localhost:9200/works/_all');

  for (let i = 0; i < works.length; ) {
    const bulk = [];

    console.time('elastic index');
    for (j = 0; j < 1000 && i < works.length; ++j) {
      let w = await getWork(works[i]);

      //console.log("bib",w);
      break;
      if (!w) {
        continue;
      }
      bulk.push(
        JSON.stringify({
          index: {_index: 'works', _id: String(works[i])}
        })
      );
      bulk.push(
        JSON.stringify({
          wid: works[i],
          name: w.name,
          creator: w.creator,
          contributor: w.creator.concat(w.contributor),
          keywords: w.keywords,
          description: w.description,
          hasCover: !!w.image,
          pids: w.hasPart.map(o => o.pid),
          additionalType: _.sortedUniq(
            w.hasPart.map(o => o.additionalType)
          ),
          datePublished: Math.min.apply(null, w.datePublished),
          series: w.isPartOf.map(o => o.name),
          popularity: get_popularity(works[i]),
          version: 1
        })
      );
      ++i;
    }
    try {
      const result = await axios.post(
        'http://localhost:9200/works/_bulk',
        bulk.join('\n') + '\n'
      );
      console.timeEnd('elastic index');
      console.log('bib', 'elastic index', i, works.length, count);
    } catch (e) {
      console.log(
        'bib',
        'ERROR elastic index error',
        JSON.stringify(e.toJSON())
      );
      console.log('bib', 'ERROR elastic index error', e.toJSON());
      process.exit(1);
      console.log('bib', e.data.error);
      throw e;
    }
  }
}
//index()
async function search(query) {
  console.time('search');
  let result;
  try {
    result = await axios.post('http://localhost:9200/works/_search', {
      size: 100,
      query: {
        function_score: {
          query: {
            multi_match: {
              query,
              fields: [
                'name^4',
                'creator^4',
                'series^2',
                'contributor^2',
                'keywords^2',
                'description' /*, 'additionalType'*/
              ]
            }
          },
          field_value_factor: {
            field: 'popularity',
            factor: 0.5,
            modifier: 'sqrt'
          }
        }
      }
    });
  } catch (e) {
    console.log('bib', e.response.data);
  }
  console.log(
    'bib',
    'result.data.hits.hits.map',
    result.data.hits.hits.map(o => {
      o = o._source;
      return `${o.wid} ${o.name} (${o.creator} ${o.contributor}) ${
        /*o.series} ${o.keywords} ${o.additionalType} ${/*o.description*/ ''
      }`;
    })
  );
  console.log('bib', 'time:', result.data.took);
  console.timeEnd('search');
}
async function similar(wid) {
  console.time('related');
  try {
    const work = (
      await axios.get('http://localhost:9200/works/_doc/' + +wid)
    ).data._source;
  } catch (e) {
    console.log('bib', 'ERROR elastic similar getwork');
    throw e;
  }
  //console.log("bib",work.data);

  const queries = [];
  if (work.name) {
    queries.push({
      match: {
        name: work.name
      }
    });
  }
  for (const creator of work.creator.concat(work.contributor)) {
    queries.push({
      match: {
        creator: creator
      }
    });
    queries.push({
      match: {
        contributor: creator
      }
    });
  }
  for (const keyword of work.keywords) {
    queries.push({
      match: {
        keywords: keyword
      }
    });
  }
  //console.log("bib",queries);
  let result;
  try {
    result = await axios.post('http://localhost:9200/works/_search', {
      size: 100,
      query: {
        bool: {
          should: queries
        }
      }
    });
  } catch (e) {
    console.log('bib', 'ERROR elastic search', queries);
    throw e;
  }
  console.log(
    'bib',
    'result.data.hits.hits',
    result.data.hits.hits
      .map(o => o._source)
      .map(o => o.wid + ' ' + o.name + ' ' + o.creator)
  );
  console.timeEnd('related');
}

async function exactCreator(creator) {
  let result;
  try {
    result = await axios.post('http://localhost:9200/works/_search', {
      size: 100,
      query: {
        function_score: {
          query: {
            match: {
              'creator.keyword': creator
            }
          },
          field_value_factor: {
            field: 'popularity',
            factor: 0.5,
            modifier: 'sqrt'
          }
        }
      },
      _source: false
    });
    result = result.data.hits.hits.map(o => +o._id);
  } catch (e) {
    console.log('bib', 'ERROR elastic exactCreaotr', e.response.data);
    throw e;
  }
  return result;
}

module.exports = {
  exactCreator
};

// Notes:
// - term statistics: // 'http://localhost:9200/works/_termvectors/12345?fields=keywords.keyword&term_statistics=true'
