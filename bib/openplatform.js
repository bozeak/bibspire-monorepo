axios = require('axios');

let openPlatformToken;
const clientId = process.env.OPENPLATFORM_CLIENTID;
const clientSecret = process.env.OPENPLATFORM_CLIENTSECRET;

const today = () => new Date().toISOString().slice(0, 10);
let coverDate = today();
let coverCache = {};

function getSpecificCover(pid) {
  if (coverDate !== today()) {
    coverCache = {};
    coverDate = today();
  }
  if (!coverCache[pid]) {
    coverCache[pid] = getSpecificCoverUncached(pid);
  }
  return coverCache[pid];
}

async function getSpecificCoverUncached(pid) {
  const [result] = await openplatform.work({
    pids: [pid],
    fields: ['coverUrlFull']
  });
  if (result && result.coverUrlFull && result.coverUrlFull[0]) {
    return result.coverUrlFull[0];
  }
  return '';
}

async function getCover(pid) {
  let cover = await getSpecificCover(pid);
  return cover;
}

function getOpenPlatformToken() {
  if (!openPlatformToken) {
    openPlatformToken = (async () =>
      (
        await axios.post(
          'https://auth.dbc.dk/oauth/token',
          'grant_type=password' + '&username=@' + '&password=@',
          {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            auth: {
              username: clientId,
              password: clientSecret
            }
          }
        )
      ).data.access_token)();
    setTimeout(
      () => (openPlatformToken = undefined),
      24 * 60 * 60 * 1000
    );
  }
  return openPlatformToken;
}

const endpoints = [
  'availability',
  'order',
  'facets',
  'search',
  'work',
  'libraries',
  'recommend',
  'renew',
  'user',
  'status',
  'storage'
];
async function op(endpoint, args) {
  let result;
  try {
    result = (
      await axios.post(
        `https://openplatform.dbc.dk/v3/${endpoint}?access_token=${await getOpenPlatformToken()}`,
        args || {}
      )
    ).data;
  } catch (e) {
    console.log('bib', 'ERROR openplatform http', endpoint, args);
    throw e;
  }
  if (result.statusCode === 200) {
    return result.data;
  } else {
    return result;
  }
}

function faust2basis(faust) {
  return `870970-basis:${
    faust >= 100000000
      ? String(faust)
      : String(100000000 + +faust).slice(1)
  }`;
}
function basis2faust(basis) {
  return +basis.split(':')[1];
}
let openplatform = {};
for (const endpoint of endpoints) {
  openplatform[endpoint] = o => op(endpoint, o);
}

let libraries;
async function getLibraries() {
  if (!libraries) {
    libraries = openplatform.libraries({});
  }
  return libraries;
}
async function getWork(pid) {
  const workReq = openplatform.work({pids: [pid]});
  const collectionReq = openplatform.work({
    pids: [pid],
    fields: ['collection']
  });
  const hasCoverReq = getSpecificCover(pid);
  const [work] = await workReq;
  const [{collection}] = await collectionReq;
  const hasCover = !!(await hasCoverReq);
  work.collection = collection;
  work.hasCover = hasCover;
  work._id = pid;
  return work;
}

module.exports = {
  openplatform,
  getWork,
  getCover,
  getLibraries
};
