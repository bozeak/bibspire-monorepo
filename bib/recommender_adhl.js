const fs = require('fs');
const readline = require('readline');
const CRC32 = require('crc-32');
const _ = require('lodash');

const {db} = require('../util/db');
const {sleep, cached} = require('../util/util');

const {getWork, workCount, pid2wid} = require('./metadata');

const _popularity = cached(
  'adhl_popularity',
  1,
  async wid =>
    (
      await db.query('SELECT COUNT(*) FROM adhl WHERE wid=?;', [wid])
    )[0][0]['COUNT(*)']
);
let wid_popularity = [];
async function popularity(wid) {
  if (wid_popularity[wid] === undefined) {
    wid_popularity[wid] = await _popularity(wid);
  }
  return wid_popularity[wid];
}
/* disabled ADHL popularity preloading
(async () => {
  const count = await workCount();
  wid_popularity = Array(count + 100);
  console.time('preload adhl popularity');
  for (let i = 0; i <= count; ) {
    const promises = [];
    for (let j = 0; j < 10; ++j) {
      promises.push(popularity(i));
      ++i;
    }
    await Promise.all(promises);
  }
  console.timeEnd('preload adhl popularity');
})();
*/

const get_sids = cached('adhl_sids', 1, async wid =>
  (
    await db.query('SELECT sid FROM adhl WHERE wid=? LIMIT 1000;', [
      wid
    ])
  )[0].map(o => o.sid)
);
const get_wids = cached('adhl_wids', 1, async sid =>
  (
    await db.query('SELECT wid FROM adhl WHERE sid=? LIMIT 1000;', [
      sid
    ])
  )[0].map(o => o.wid)
);
module.exports = {
  get_wids,
  get_sids,
  popularity
};

//
// data initialisation
//
async function insertAdhl(filename) {
  const fileStream = fs.createReadStream(filename);
  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
  });

  let i = 0;
  let t0 = Date.now();
  let rows = [];
  const count = 0;
  async function insert() {
    if (!rows.length) {
      return;
    }
    const values = rows;
    rows = [];
    try {
      await db.query('INSERT IGNORE INTO adhl (sid, d, wid) VALUES ?', [
        values
      ]);
    } catch (e) {
      console.log('bib', e);
    }
    if (i % 100000 === 0) {
      const totalCount = 238600000;
      const dt = Date.now() - t0;
      console.log(
        'bib',
        `adhl inserted ${
          values.length
        } x10 in ${dt} ms, total: ${i} / ${totalCount} – ETA: ${((totalCount -
          i) *
          (dt / 100000)) /
          60 /
          60 /
          1000}`
      );
      t0 = Date.now();
    }
  }
  let promises = [];
  for await (const line of rl) {
    if (++i % 10000 === 0) {
      await Promise.all(promises);
      promises = [];
      await insert();
    }
    const o = JSON.parse(line);
    promises.push(
      (async () => {
        try {
          const wid = await pid2wid(o.materiale_id);
          if (!wid) {
            return;
          }
          const sid = CRC32.str(o.laaner_id + o.dato.slice(0, 4));
          rows.push([sid, o.dato, wid]);
        } catch (e) {
          console.log('bib', 'adhl error fetching pid:', pid);
        }
      })()
    );
  }
  await Promise.all(promises);
  await insert();

  console.log('bib', 'cleanup adhl', new Date().toISOString());
  console.time('cleanup adhl');
  await db.execute(
    'CREATE OR REPLACE TABLE adhl_cleanup SELECT sid, COUNT(wid) AS wids FROM adhl GROUP BY sid;'
  );
  await db.execute('DELETE FROM adhl_cleanup WHERE wids>1;');
  await db.execute(
    'DELETE adhl, adhl_cleanup FROM adhl INNER JOIN adhl_cleanup WHERE adhl_cleanup.sid=adhl.sid AND adhl_cleanup.wids=1;'
  );
  await db.execute('DROP TABLE adhl_cleanup;');
  console.timeEnd('cleanup adhl');
}
(async () => {
  /*
  await sleep(1000);
  //console.log("bib",'loading ADHL');
    await insertAdhl(__dirname +'/incoming/adhl.jsonl');
  console.time('adhl flush cache');
  console.timeEnd('adhl flush cache');
  */
})();
