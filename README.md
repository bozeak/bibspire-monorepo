# Veduz monorepos

This is a mono-repository, that contains the source code 

- `bib/` BibSpire backend 
- `logging/` aggregate logs to mysql, - backend for dashboard
- `server/` server / backend code
- `vorkal/` vores kalender .dk
- `util/` various utility code (both frontend and backend)
- `widgets/` frontends using the backend
    - `ddbcms` BibSpire widget
    - `dash` status dashboard


Development:

- make sure mysql + redis + elastic is running
- `scripts/dev.sh`

# Tasks

- delete tables creator/id keywords/id series/id work holdings
- token+permission table for auth, instead of tokens in auth table...
    - token, created, expires, permissions, reference
- clean bib-up database
- merge kulturdata into veduz
- merge search-proof-of-concept into veduz
- new (general/simplified) recommender
- minimal mycrud
- custom recommend pages
- better logging: date, type-byte, subtype string, 4byte time, content
- get vorkal up and running again
- db-update: enable vejle holding-filtering, when holdings received from DBC.

# Changelog
## 2019 May
- bib: new recommender based on webtrekk data
- bib: filtering on holdings
- bib: dkabm to schema.org conversion
- bib: cached work data lookup
- bib: indexing keywords / series / work / ...
- bib: endpoint for uploading holdings
- ddbcms: new design for recommendation widget
## 2019 June
- bib: new recommender version, handling "værkmatch"
- bib: use ADHL for older materials
- bib: endpoint for uploading webtrekk-data
- bib: include eReol-materials in addition to physical holding
- bib: filtering on hasCover
- ddbcms: IE11 support on recommender widget
- ddbcms: configurable number of rows
- ddbcms: responsive load, ie. only add as many recommendations to html as actually shown.
- ddbcms: configurable view
## 2019 July
- bib: recommendations from author when to usagedata
- bib: API available for others, requires token
- bib: api-documentation
- bib: performance-test: approx handle 1000reqs/minute
- refactoring
## 2019 August
- ddbcms: recommendations in user status
- bib: refactoring
- bib: elastic-search
- bib: recommendations from several pids
- bib: stable api, v1.1 released
## 2019 September
- ddbcms: better mobile view
- ddbcms: recommendations on work-view
- ddbcms: better presentation on user status
- bib: support different recommenders, i.e. adhl+webtrekk, webtrekk, dbc
- bib: use holdings from dbc
- vorkal: automatic screenshot api
- vorkal: proof of concept
## 2019 October
- dash: stat dashboard
## 2019 November
- vorkal: remove, as out of sync
- kulturdata: hack4dk prototype
- bib: search experiment / proof-of-concept
## 2019 December
- switch to monorepos
## 2020 February
- dash: merge dashboard into repository
- update changelog/backlog
- clean up bib-metadata no extra indexing
# Setup

- production-server
- staging-server
    - daily sync from production server
    - backup
    - webhook that pulls + rebuilds + tests + deploys on gitlab commit
- dev setup
    - install + configure:  mariadb + elasticsearch + redis
    - restore mariadb backup

- mariadb-DB
- /var/www
- /etc/nginx
- certbot/etc

infrastructure:
- nginx
- adminer
- wordpress

general:

```
sudo apt install -y inotify-tools
```

## /data

```
sudo rsync -avz bibdata.dk:/data /data
```

## MariaDB

Install mariadb
```
sudo apt-get install -y software-properties-common
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo add-apt-repository 'deb http://mirrors.n-ix.net/mariadb/repo/10.3/ubuntu bionic main'
sudo apt-get install -y mariadb-server mariadb-client mariadb-backup mariadb-plugin-rocksdb
```

/etc/mysql/mariadb.cnf:

```
innodb_buffer_pool_size = 16G
plugin_load_add = ha_rocksdb
rocksdb-override-cf-options='cf1={compression=LZ4;bottommost_compression=Zlib;}'
```

```
sudo systemctl stop mariadb
 # sudo rsync -avz --delete root@bibdata.dk:/backup/mariadb/ /var/lib/mysql
sudo chown -R mysql:mysql /var/lib/mysql
sudo systemctl start mariadb
```


## Elastic
```
sudo wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
sudo echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
sudo apt-get -y update 
sudo apt-get -y install elasticsearch
sudo systemctl daemon-reload
sudo systemctl start elasticsearch

curl -X DELETE 'http://localhost:9200/_all'
```

## Redis

```
sudo apt-get -y install redis
sudo systemctl stop redis
sudo sh -c 'rm -rf /var/lib/redis/*'
sudo systemctl start redis
sudo vim /etc/redis/redis.conf
```


```
maxmemory 16gb
maxmemory-policy allkeys-lfu
```

# Misc

Copyright 2019-2020 Veduz ApS
