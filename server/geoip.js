const axios = require('axios');

const {db} = require('../util/db');
const {sleep} = require('../util/util');

async function getCached(ip) {}
const queue = [];
let running = false;
async function geoLoop() {
  if (running) {
    return;
  }
  running = true;
  while (queue.length) {
    const [ip, resolve, reject] = queue.pop();
    try {
      let result = (
        await db.query(
          `SELECT location FROM geoip_cache WHERE ip=INET_ATON(?);`,
          [ip]
        )
      )[0][0];
      if (result) {
        resolve(JSON.parse(result.location));
      } else {
        result = (await axios.get('https://freegeoip.app/json/' + ip))
          .data;
        delete result.ip;
        db.execute(
          `INSERT IGNORE INTO geoip_cache (ip, location) VALUES (INET_ATON(?), ?);`,
          [ip, JSON.stringify(result)]
        );
        resolve(result);
        await sleep(500); // obey rate limit
      }
    } catch (e) {
      console.log(e);
    }
  }
  running = false;
}
function geoip(ip) {
  const result = new Promise((resolve, reject) =>
    queue.push([ip, resolve, reject])
  );
  geoLoop();
  return result;
}
module.exports = {geoip};
