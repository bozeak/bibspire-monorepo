openapi: 3.0.0
servers:
  - description: BibData
    url: https://bibdata.dk/v1
  - description: Local development server
    url: http://localhost:3000/v1
info:
  description: |
    Various APIs developed by Veduz ApS, including:

    - [BibSpire](https://bibspire.dk) APIs, such as recommender for danish libraries
    - Internal APIs
    - APIs for vores kalender
    
    The library APIs uses, and builds on, data from openplatform.dbc.dk – so data are under the same license as those, and projects using the data might be required to be a part of the danish public library sector.
    
    Get in touch to get a token to get access to the APIs.
  version: "1.1.0"
  title: Veduz APIs
tags:
  - name: BibData
    description: Public / stable APIs related to the BibSpire project, usable by others.
  - name: internal
    description: Internal APIs – subject to change without notice. Do not use without prior permissions.
paths:
  /recommend:
    get:
      tags: ["BibData"]
      summary: Get related materials from recommender engine.
      operationId: recommend
      parameters:
        - name: pid
          in: query
          description: |
             PIDs to find relatede materials for. 
             
             A PID is an id of a danish library material, such as: 870970-basis:54966091
          explode: true
          schema:
            type: array
            example: ["870970-basis:53030955"]
            items:
              type: string
        - name: site
          in: query
          description: |
            Domain for the recommendation, such as the domain for the DDB-CMS website. This is used to determine the recommendation configuration.
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Recommendations'
      security:
        - token: []
  '/object/{pid}':
    get:
      tags: ["BibData"]
      summary: Get metadata for a single object/manifest.
      operationId: getObject
      parameters:
        - name: pid
          example: 870970-basis:50746445
          in: path
          description: PID for material to lookup.
          required: true
          schema:
            type: string
        - name: format
          description: |
            The format of the meta data, – this can either be in DKABM in JSON-LD as openplatform.dbc.dk returns, or schema.org in JSON-LD as described on [https://bibspire.dk/dkabm-til-schema-org](https://bibspire.dk/dkabm-til-schema-org).
          in: query
          schema: 
            type: string
            enum:
            - dkabm
            - schema.org
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Object'
      security:
        - token: []
  '/collection/{wid}':
    get:
      tags: ["BibData"]
      summary: Get metadata for a single work/collection.
      operationId: getCollection
      parameters:
        - name: wid
          in: path
          description: Work ID
          required: true
          schema:
            type: number
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Collection'
      security:
        - token: []
  '/libraries':
    get:
      tags: ["internal"]
      security: {token: []}
  '/randomPid':
    get:
      tags: ["internal"]
      security: {token: []}
#  /holdings:
#    put:
#      summary: Upload library holdings.
#      operationId: holdings
#      parameters:
#        - name: agency
#          in: query
#          required: true
#          description: |
#            Agency id for the library. Must match the token.
#          schema:
#            type: number
#      requestBody:
#        description: |
#          CSV file from [https://reports.cicero-fbs.com/reports/report/Reports/Folkebiblioteker/bestand](https://reports.cicero-fbs.com/reports/report/Reports/Folkebiblioteker/bestand). See [https://bibspire.dk/installation](https://bibspire.dk/installation) for details, about how to extract this.
#        required: true
#        content:
#          text/csv:
#            schema:
#              type: string
#      responses:
#        '200':
#          description: ok
##      security:
#        - token: []
components:
  schemas:
    Object:
      type: object
      properties:
        "@type":
          type: string
        name:
          type: string
        alternativeHeadline:
          type: string
        alternateName:
          type: array
          items:
            type: string
        image:
          type: string
        creator:
          type: array
          items:
            type: string
        contributor:
          type: array
          items:
            type: string
        url:
          type: string
        sameAs:
          type: array
          items:
            type: string
        datePublished:
          type: string
        keywords:
          type: array
          items:
            type: string
        description:
          type: array
          items:
            type: string
        identifier:
          type: array
          items:
            type: string
        inLanguage:
          type: array
          items:
            type: string
        subjectOf:
          type: array
          items:
            type: string
        isPartOf:
          type: array
          items:
            type: object
            properties:
              url:
                type: string
              name:
                type: string
        publisher: 
          type: string
        version:
          type: string
        additionalType:
          type: string
        typicalAgeRange:
          type: string
        pid:
          description: "Danish library id, – not part of schema.org"
          type: string
        wid:
          description: "BibData work/collection id, – not part of schema.org"
          type: string
    Collection:
      type: object
      properties:
        "@type":
          type: string
        name:
          type: string
        image:
          type: string
        creator:
          type: array
          items:
            type: string
        contributor:
          type: array
          items:
            type: string
        url:
          type: string
        keywords:
          type: array
          items:
            type: string
        description:
          type: array
          items:
            type: string
        isPartOf:
          type: array
          items:
            type: object
            properties:
              name:
                type: string
              url:
                type: string
        hasPart:
          type: object
          properties:
            "@type":
              type: string
            url:
              type: string
            image:
              type: string
            datePublished:
              type: string
            identifier:
              type: array
              items:
                type: string
            additionalType:
              type: string
            sameAs:
              type: array
              items:
                type: string
            pid:
              type: string
        datePublished:
          type: array
          items:
            type: number
        wid:
          type: number
    Recommendations:
      type: object
      properties:
        related:
          type: array
          items:
            type: object
            properties:
              name:
                type: string
              image:
                type: string
              creator:
                type: array
                items:
                  type: string
              hasPart:
                type: array
                items:
                  type: object
                  properties:
                    pid:
                      type: string
                    datePublished:
                      type: string
                    additionalType:
                      type: string
              wid:
                type: number
        config:
          type: object
  securitySchemes:
    token:
      type: apiKey
      name: token
      in: query
