/*! DDB-CMS recommender widget, copyright Veduz ApS */

// Imports
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import React from 'react';
import ReactDOM from 'react-dom';
import Carousel from './Carousel.jsx';
import SidebarList from './SidebarList.jsx';
import axios from 'axios';
const _ = {
  uniq: require('lodash/uniq')
};

// Path matches for different dispatch/handling
const urlObjectMatch = location.href.match(/\/ting\/object\/([^?#.]*)/);
const urlCollectionMatch = location.href.match(
  /\/ting\/collection\/([^?#.]*)/
);
const urlStatusLoanMatch = location.href.match(/\/user\/me\/status/);
const urlEndFullPidMatch = location.href.match(
  /full\/([0-9]+-[^%:]+(:|%3[aA])[0-9a-z_]+)([#?].*)?$/
);

// Preload recommendations on script load time
let recommendationsPromise;
if (urlObjectMatch) {
  recommendationsPromise = getRecommendations([urlObjectMatch[1]]);
} else if (urlCollectionMatch) {
  recommendationsPromise = getRecommendations([urlCollectionMatch[1]]);
} else if ( urlEndFullPidMatch) {
  recommendationsPromise = getRecommendations([urlEndFullPidMatch[1]]);
}

async function getRecommendations(pids) {
  const result = (
    await axios.get(
      `https://api.bibspire.dk/v1/recommend?${pids
        .map(s => 'pid=' + s)
        .join('&')}&site=${location.host}&token=l5ICyj27g8QeQcus`
    )
  ).data;

  return {
    related: result.related || [],
    config: (result.config && result.config.tingObject) || {}
  };
}
function addReactChild(parent, content) {
  if (parent) {
    const elem = document.createElement('div');
    parent.appendChild(elem);
    ReactDOM.render(content, elem);
  }
}

async function widgetView() {
  for (const elem of document.querySelectorAll('.bibspire-widget')) {
    let pids = String(elem.dataset.pid)
      .split(' ')
      .filter(o => o);
    const {related, config} = await getRecommendations(pids);
    if (related.length) {
      addReactChild(
        elem,
        <Carousel
          related={related}
          config={config}
          title={title(elem)}
        />
      );
    }
  }
}
const title = elem =>
  (elem && elem.dataset && elem.dataset.title) || 'Inspiration';
async function objectView() {
  let {related, config} = await recommendationsPromise;
  if (related.length) {
    addReactChild(
      document.querySelector('#ting-recommender') ||
        document.querySelector('.ting-object'),
      <Carousel
        related={related}
        config={config}
        title={title(document.querySelector('#ting-recommender'))}
      />
    );
  }
}
async function collectionView() {
  let {related, config} = await recommendationsPromise;

  if (related.length) {
    if (window.innerWidth <= 950) {
      addReactChild(
        document.querySelector('.primary-content'),
        <div style={{clear: 'both', paddingTop: 120}}>
          <Carousel
            related={related}
            config={config}
            title={title(document.querySelector('#ting-recommender'))}
          />
        </div>
      );
    } else {
      addReactChild(
        document.querySelector('.secondary-content'),
        <div style={{marginTop: 150}}>
          <SidebarList related={related} src="collection" />
        </div>
      );
    }
  }
}
async function loanStatusView() {
  let loans = _.uniq(
    Array.from(document.querySelectorAll('.primary-content a'))
      .map(o => o.href)
      .filter(s => s.match(/ting\/object\//))
      .map(o =>
        o.replace(/.*\/ting\/object\/([^?#]*).*/, (_, _1) =>
          decodeURIComponent(_1)
        )
      )
  );
  let {related, config} = await getRecommendations(loans);

  if (related.length) {
    if (window.innerWidth <= 950) {
      addReactChild(
        document.querySelector('.primary-content'),
        <div style={{clear: 'both', paddingTop: 120}}>
          <Carousel
            related={related}
            config={config}
            title={title(document.querySelector('#ting-recommender'))}
          />
        </div>
      );
    } else {
      addReactChild(
        document.querySelector('.secondary-content'),
        <SidebarList related={related} src="userstatus" />
      );
    }
  }
}
async function devView() {
  const pid = '870970-basis:54970021';
  const {related, config} = await getRecommendations([pid]);
  addReactChild(
    document.body,
    <Carousel related={related} config={config} title="Inspiration" />
  );
}

// Execute correct view on dom load
let started = false;
function attach() {
  if (started) {
    return;
  }
  started = true;
  if (
    urlObjectMatch ||
    (document.querySelector('#ting-recommender') && urlEndFullPidMatch)
  ) {
    objectView();
  } else if (urlCollectionMatch) {
    collectionView();
  } else if (urlStatusLoanMatch) {
    loanStatusView();
  } else if(
    document.querySelector('#ting-recommender')
    && document.querySelector('#ting-recommender').dataset
    && document.querySelector("#ting-recommender").dataset.tingObjectId
  ) {
    recommendationsPromise = getRecommendations([document.querySelector("#ting-recommender").dataset.tingObjectId]);
    objectView();
  }
  if (document.querySelector('.bibspire-widget')) widgetView();
  if (location.hash === '#bibspire-layout-demo') devView();
}
if (
  document.readyState === 'complete' ||
  document.readyState === 'interactive'
) {
  attach();
} else {
  document.addEventListener('DOMContentLoaded', attach);
}
