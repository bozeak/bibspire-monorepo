import React from 'react';
import ReactDOM from 'react-dom';

let SidebarList = ({related, src}) => (
  <div>
    <h2 style={{marginTop: 72}} className="sub-menu-title">
      Prøv også:
    </h2>
    {related
      .slice(0, 10)
      .map(({name, image, creator, hasPart, wid}) => (
        <a
          key={wid}
          style={{marginBottom: 20, display: 'block'}}
          href={
            '/ting/object/' +
            hasPart[0].pid +
            '?u_navigatedby=bibspire_' +
            src
          }
        >
          <img
            style={{
              verticalAlign: 'top',
              display: 'inline',
              opacity: 0.7,
              marginRight: 10
            }}
            src={image}
            width={80}
          />
          <span
            style={{
              verticalAlign: 'top',
              display: 'inline-block',
              width: 180
            }}
          >
            <h3
              className="item-title has-message"
              style={{margin: 0, padding: 0}}
            >
              {name}
            </h3>
            <div className="item-creators">{creator && creator[0]}</div>
          </span>
        </a>
      ))}
  </div>
);

export default SidebarList;
